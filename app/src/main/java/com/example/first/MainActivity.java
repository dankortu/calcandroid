package com.example.first;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.example.first.databinding.ActivityMainBinding;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding viewBinding;
    ArrayList<Double> operands = new ArrayList<>();
    String usingOperator = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.my_gradient);

        viewBinding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(viewBinding.getRoot());

        viewBinding.btnEqually.setOnClickListener(this::handleEquallyClick);
        viewBinding.btnClear.setOnClickListener(this::handleClearClick);
        viewBinding.btnPlus.setOnClickListener(this::handleOperatorClick);
        viewBinding.btnMinus.setOnClickListener(this::handleOperatorClick);
        viewBinding.btnDivision.setOnClickListener(this::handleOperatorClick);
        viewBinding.btnMultiplication.setOnClickListener(this::handleOperatorClick);


        viewBinding.btnDot.setOnClickListener(this::handleClickDot);
        viewBinding.btnZero.setOnClickListener(this::handleClickNum);
        viewBinding.btnOne.setOnClickListener(this::handleClickNum);
        viewBinding.btnTwo.setOnClickListener(this::handleClickNum);
        viewBinding.btnThree.setOnClickListener(this::handleClickNum);
        viewBinding.btnFour.setOnClickListener(this::handleClickNum);
        viewBinding.btnFive.setOnClickListener(this::handleClickNum);
        viewBinding.btnSix.setOnClickListener(this::handleClickNum);
        viewBinding.btnSeven.setOnClickListener(this::handleClickNum);
        viewBinding.btnEight.setOnClickListener(this::handleClickNum);
        viewBinding.btnNine.setOnClickListener(this::handleClickNum);

        viewBinding.btnPlus.setOnClickListener(this::handleOperatorClick);
    }

    private void handleClickDot(View view) {
        String currentOperand = viewBinding.inputField.getText().toString();
        if (!currentOperand.isEmpty()) {
            if(!currentOperand.contains(".")){
                viewBinding.inputField.setText(currentOperand.concat(((TextView) view).getText().toString()));
            }

        } else {
            viewBinding.inputField.setText("0.");
        }

    }


    private void handleEquallyClick(View view) {
        if (!"".equals(usingOperator) && !"".equals(viewBinding.inputField.getText().toString())) {
            operands.add(Double.parseDouble(viewBinding.inputField.getText().toString()));
            viewBinding.outputField.setText(getResult(usingOperator));
            viewBinding.inputField.setText("");
        }
    }

    private void handleClearClick(View view) {
        operands.clear();
        usingOperator = "";
        viewBinding.inputField.setText("");
        viewBinding.outputField.setText("");
    }


    private void handleOperatorClick(View view) {

        if (!"".equals(viewBinding.inputField.getText().toString())) {
            operands.add(Double.parseDouble(viewBinding.inputField.getText().toString()));

            if (usingOperator != null) {
                String result = getResult(usingOperator);
                viewBinding.outputField.setText(result);
            }
        }
        usingOperator = ((TextView) view).getText().toString();
        viewBinding.inputField.setText("");
    }


    private void handleClickNum(View view) {
        if ("0".equals(viewBinding.inputField.getText().toString())) {
            viewBinding.inputField.setText(((TextView) view).getText().toString());
        } else {
            final String text = viewBinding.inputField
                    .getText()
                    .toString()
                    .concat(((TextView) view).getText().toString());
            viewBinding.inputField.setText(text);
        }

    }




    @SuppressLint("SetTextI18n")
//    private void handleClick(View view) {
//        TextView btn = (TextView) view;
//
//        String btnValue = (String) btn.getText();
//        String currentText = (String) viewBinding.inputField.getText();
//        String lastSymbol = "";
//
//        boolean lastSymbolIsOperator = false;
//        boolean btnIsOperator = operatorsList.contains(btnValue);
//        boolean btnIsNum = numsList.contains(btnValue);
//
//        if (currentText.length() > 0) {
//            lastSymbol = Character.toString(currentText.charAt(currentText.length() - 1));
//            lastSymbolIsOperator = operatorsList.contains(lastSymbol);
//        }
//
//        if (btnIsNum || (btnValue.equals(".") && !currentOperand.contains(".") && currentOperand.length() > 0)) {
//
//            if ("0".equals(currentOperand) || "0".equals(currentText)){
//                currentOperand = btnValue;
//                viewBinding.inputField.setText(currentText.substring(0, currentText.length()-1) + btnValue);
//            } else {
//                currentOperand = currentOperand + btnValue;
//                viewBinding.inputField.setText(currentText + btnValue);
//            }
//        } else if (btnIsOperator && currentText.length() > 0 && !"=".equals(btnValue)) {
//
//            if (!"".equals(currentOperand)  ) {
//                operands.add(currentOperand);
//            }
//
//            currentOperand = "";
//
//
//            if ("".equals(currentOperand)) {
//                viewBinding.inputField.setText(currentText + btnValue);
//            }
//            if (operands.size() > 0 && usingOperator.length() == 0) {
//                usingOperator = btnValue;
//            }
//
//            if (lastSymbolIsOperator || lastSymbol.equals(".")) {
//
//                viewBinding.inputField.setText(currentText.substring(0, currentText.length() - 1) + btnValue);
//                usingOperator = btnValue;
//                System.out.println("Оператор изменился на " + usingOperator);
//            }
//
//
//        } else if ((lastSymbolIsOperator || "".equals(currentText)) && btnValue.equals(".")) {
//            viewBinding.inputField.setText(currentText + "0.");
//        } else if (btnValue.equals("C")) {
//            viewBinding.inputField.setText("");
//            operands.clear();
//            usingOperator = "";
//        }
//
//        if ("=".equals(btnValue) && !".".equals(lastSymbol) && !"=".equals(lastSymbol) && !"".equals(currentOperand)) {
//            operands.add(currentOperand);
//            currentOperand = "";
//            getResult("");
//        } else {
//            getResult(btnValue);
//        }
//
//
//    }


    String getResult(String btnValue) {
        String resultString = "";

        if (operands.size() == 2) {
            double result;

            System.out.println("Числа: " + operands + "Оператор: " + usingOperator);

            switch (usingOperator) {
                case ("+"):
                    result = operands.get(0) + operands.get(1);
                    resultString = Double.toString(result);
                    break;
                case ("-"):
                    result = operands.get(0) - operands.get(1);
                    resultString = Double.toString(result);
                    break;
                case ("x"):
                    if (operands.get(0)==0.0 || operands.get(1)==0.0 ) {
                        resultString="0";
                    } else{
                        result = operands.get(0) * operands.get(1);
                        resultString = Double.toString(result);
                    }

                    break;
                case ("/"):

                    result = operands.get(0) / operands.get(1);
                    resultString = Double.toString(result);

                    break;
            }

            if (resultString.endsWith(".0")) {
                resultString = resultString.substring(0, resultString.length() - 2);
            }

            operands.clear();
            operands.add(Double.parseDouble(resultString));

            usingOperator = btnValue;

        }
        return resultString;
    }

}